﻿using System;

namespace Notebook
{
    class Note
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Telephone { get; set; }
        public string Country { get; set; }

        public string Patronymic { get; set; }
        public DateTime? BirthdayDate { get; set; }
        public string Organization { get; set; }
        public string Position { get; set; }
        public string Other { get; set; }

        public Note(int id, string name, string surname, string number, string country)
        {
            this.Id = id;
            this.Name = name;
            this.Surname = surname;
            this.Telephone = number;
            this.Country = country;
        }
    }
}
