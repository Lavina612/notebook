﻿using System;
using System.Collections.Generic;

namespace Notebook
{
	class Notebook
	{
		static void Main(string[] args)
		{
			List<Note> notes = new List<Note>();
			int idNewNote = 1;
			Console.WriteLine("Добро пожаловать в Notepad!");
			Console.WriteLine();
			CreateNewNote(notes, ref idNewNote);
			string selected = "";
			while (selected != "6")
			{
				Console.WriteLine();
				Console.WriteLine("-----------------------------------------------------------------------");
				Console.WriteLine();
				Console.WriteLine("Что Вы хотите сделать теперь? (нажмите соответствующую цифру)");
				Console.WriteLine("1 - Создать новую запись");
				Console.WriteLine("2 - Редактировать созданную запись");
				Console.WriteLine("3 - Удалить созданную запись");
				Console.WriteLine("4 - Посмотреть конкретную запись");
				Console.WriteLine("5 - Посмотреть все созданные записи (краткий формат)");
				Console.WriteLine("6 - Выйти из программы");
				selected = Console.ReadLine();
				switch (selected)
				{
					case "1":
						CreateNewNote(notes, ref idNewNote);
						break;
					case "2":
						EditNote(notes);
						break;
					case "3":
						DeleteNote(notes);
						break;
					case "4":
						ShowNote(notes);
						break;
					case "5":
						ShowAllNotes(notes);
						break;
					case "6":
						Console.WriteLine("До скорой встречи!");
						Console.WriteLine();
						break;
					default:
						Console.WriteLine("Вы ввели что-то не то. Попробуйте ещё раз!");
						break;
				}
			}
		}

		static void CreateNewNote(List<Note> notes, ref int idNewNote)
		{
			Console.WriteLine("Давайте создадим новую запись.");
			string name = "";
			string surname = "";
			string phone = "";
			string country = "";
			string date = "";
			while (name == "")
			{
				Console.WriteLine("Введите имя:");
				name = Console.ReadLine().Trim();
				if (name == "")
				{
					Console.WriteLine("Имя не может быть пустым. Попробуйте ещё раз.");
					name = "";
				}
			}
			while (surname == "")
			{
				Console.WriteLine("Введите фамилию:");
				surname = Console.ReadLine().Trim();
				if (surname == "")
				{
					Console.WriteLine("Фамилия не может быть пустой. Попробуйте ещё раз.");
					surname = "";
				}
			}
			while (phone == "")
			{
				Console.WriteLine("Введите номер телефона (только цифры):");
				phone = Console.ReadLine().Trim();
				if (phone == "")
				{
					Console.WriteLine("Номер телефона не может быть пустым. Попробуйте ещё раз.");
					phone = "";
					continue;
				}
				if (!long.TryParse(phone, out long num))
				{
					Console.WriteLine("Номер телефона не соответствует формату. Попробуйте ещё раз.");
					phone = "";
				}
			}
			while (country == "")
			{
				Console.WriteLine("Введите страну:");
				country = Console.ReadLine().Trim();
				if (country == "")
				{
					Console.WriteLine("Страна не может быть пустой. Попробуйте ещё раз.");
					country = "";
				}
			}
			Note note = new Note(idNewNote, name, surname, phone, country);

			Console.WriteLine("Введите отчество (можете оставить пустым):");
			note.Patronymic = Console.ReadLine();
			while (date == "")
			{
				Console.WriteLine("Введите дату рождения - формат ДД.ММ.ГГГГ (можете оставить пустым):");
				date = Console.ReadLine();
				if (date == "") break;
				if (!DateTime.TryParse(date, out DateTime db))
				{
					Console.WriteLine("Дата рождения не соответствует формату. Попробуйте ещё раз.");
					date = "";
				}
				else note.BirthdayDate = db.Date;
			}
			Console.WriteLine("Введите организацию (можете оставить пустым):");
			note.Organization = Console.ReadLine();
			Console.WriteLine("Введите должность (можете оставить пустым):");
			note.Position = Console.ReadLine();
			Console.WriteLine("Вы также можете ввести прочие заметки (можете оставить пустыми):");
			note.Other = Console.ReadLine();
			notes.Add(note);
			Console.WriteLine();
			Console.WriteLine("Отлично! Новая учётная запись создана и добавлена в записную книжку!");
			Console.WriteLine("Доступ к ней будет осущесвляться по идентификатору " + idNewNote);
			idNewNote++;
		}

		static void EditNote(List<Note> notes)
		{
			int index = -1;
			while (index == -1)
			{
				Console.WriteLine("Введите идентификатор записи, которую хотите отредактировать");
				if (int.TryParse(Console.ReadLine(), out int id))
				{
					for (int i = 0; i < notes.Count; i++)
					{
						if (notes[i].Id == id)
						{
							index = i;
							break;
						}
					}
					if (index == -1)
					{
						Console.WriteLine("Записи с таким идентификатором не существует. Попробуйте ещё раз.");
					}
				}
				else
				{
					Console.WriteLine("Вы ввели что-то не то. Попробуйте ещё раз.");
				}

			}
			Note note = notes[index];
			string number = "";
			while (number != "10")
			{
				Console.WriteLine();
				Console.WriteLine("Что Вы хотите изменить? (нажмите соответствующую цифру)");
				Console.WriteLine("1 - имя");
				Console.WriteLine("2 - фамилию");
				Console.WriteLine("3 - номер телефона");
				Console.WriteLine("4 - страну");
				Console.WriteLine("5 - отчество");
				Console.WriteLine("6 - дату рождения");
				Console.WriteLine("7 - организацию");
				Console.WriteLine("8 - должность");
				Console.WriteLine("9 - прочие заметки");
				Console.WriteLine("10 - отмена");
				number = Console.ReadLine();
				switch (number)
				{
					case "1":
						Console.WriteLine("Предыдущий вариант: " + note.Name);
						string name = "";
						while (name == "")
						{
							Console.Write("Новый вариант: ");
							name = Console.ReadLine().Trim();
							if (name == "")
							{
								Console.WriteLine("Имя не может быть пустым. Попробуйте ещё раз.");
								name = "";
							}
						}
						note.Name = name;
						Console.WriteLine("Запись успешно изменена!");
						break;
					case "2":
						Console.WriteLine("Предыдущий вариант: " + note.Surname);
						string surname = "";
						while (surname == "")
						{
							Console.Write("Новый вариант: ");
							surname = Console.ReadLine().Trim();
							if (surname == "")
							{
								Console.WriteLine("Фамилия не может быть пустой. Попробуйте ещё раз.");
								surname = "";
							}
						}
						note.Surname = surname;
						Console.WriteLine("Запись успешно изменена!");
						break;
					case "3":
						Console.WriteLine("Предыдущий вариант: " + note.Telephone);
						string phone = "";
						while (phone == "")
						{
							Console.Write("Новый вариант: ");
							phone = Console.ReadLine().Trim();
							if (phone == "")
							{
								Console.WriteLine("Номер телефона не может быть пустым. Попробуйте ещё раз.");
								phone = "";
								continue;
							}
							if (!long.TryParse(phone, out long num))
							{
								Console.WriteLine("Номер телефона не соответствует формату. Попробуйте ещё раз.");
								phone = "";
							}
						}
						note.Telephone = phone; ;
						Console.WriteLine("Запись успешно изменена!");
						break;
					case "4":
						Console.WriteLine("Предыдущий вариант: " + note.Country);
						string country = "";
						while (country == "")
						{
							Console.Write("Новый вариант: ");
							country = Console.ReadLine().Trim();
							if (country == "")
							{
								Console.WriteLine("Страна не может быть пустой. Попробуйте ещё раз.");
								country = "";
							}
						}
						note.Country = country;
						Console.WriteLine("Запись успешно изменена!");
						break;
					case "5":
						Console.WriteLine("Предыдущий вариант: " + note.Patronymic);
						Console.Write("Новый вариант: ");
						note.Patronymic = Console.ReadLine();
						Console.WriteLine("Запись успешно изменена!");
						break;
					case "6":
						Console.WriteLine("Предыдущий вариант: " + note.BirthdayDate);
						string date = "";
						while (date == "")
						{
							Console.Write("Новый вариант: ");
							date = Console.ReadLine();
							if (date == "") break;
							if (!DateTime.TryParse(date, out DateTime db))
							{
								Console.WriteLine("Дата рождения не соответствует формату. Попробуйте ещё раз.");
								date = "";
							}
							else note.BirthdayDate = db.Date;
						}
						Console.WriteLine("Запись успешно изменена!");
						break;
					case "7":
						Console.WriteLine("Предыдущий вариант: " + note.Organization);
						Console.Write("Новый вариант: ");
						note.Organization = Console.ReadLine();
						Console.WriteLine("Запись успешно изменена!");
						break;
					case "8":
						Console.WriteLine("Предыдущий вариант: " + note.Position);
						Console.Write("Новый вариант: ");
						note.Position = Console.ReadLine();
						Console.WriteLine("Запись успешно изменена!");
						break;
					case "9":
						Console.WriteLine("Предыдущий вариант: " + note.Other);
						Console.Write("Новый вариант: ");
						note.Other = Console.ReadLine();
						Console.WriteLine("Запись успешно изменена!");
						break;
					case "10":
						break;
					default:
						Console.WriteLine("Вы ввели что-то не то. Попробуйте ещё раз.");
						break;
				}
			}
			notes.RemoveAt(index);
			notes.Insert(index, note);
		}

		static void DeleteNote(List<Note> notes)
		{
			int index = -1;
			while (index == -1)
			{
				Console.WriteLine("Введите идентификатор записи, которую хотите удалить");
				if (int.TryParse(Console.ReadLine(), out int id))
				{
					for (int i = 0; i < notes.Count; i++)
					{
						if (notes[i].Id == id)
						{
							index = i;
							break;
						}
					}
					if (index == -1)
					{
						Console.WriteLine("Записи с таким идентификатором не существует. Попробуйте ещё раз.");
					}
				}
				else
				{
					Console.WriteLine("Вы ввели что-то не то. Попробуйте ещё раз.");
				}
			}
			Note note = notes[index];
			notes.RemoveAt(index);
			Console.WriteLine();
			Console.WriteLine("Запись успешно удалена.");
		}

		static void ShowNote(List<Note> notes)
		{
			int index = -1;
			while (index == -1)
			{
				Console.WriteLine("Введите идентификатор записи, которую хотите посмотреть");
				if (int.TryParse(Console.ReadLine(), out int id))
				{
					for (int i = 0; i < notes.Count; i++)
					{
						if (notes[i].Id == id)
						{
							index = i;
							break;
						}
					}
					if (index == -1)
					{
						Console.WriteLine("Записи с таким идентификатором не существует. Попробуйте ещё раз.");
					}
				}
				else
				{
					Console.WriteLine("Вы ввели что-то не то. Попробуйте ещё раз.");
				}
			}
			Note note = notes[index];
			Console.WriteLine();
			Console.WriteLine("Идентификатор: " + note.Id);
			Console.WriteLine("Фамилия: " + note.Surname);
			Console.WriteLine("Имя: " + note.Name);
			Console.WriteLine("Отчество: " + note.Patronymic);
			Console.WriteLine("Номер телефона: " + note.Telephone);
			Console.WriteLine("Страна: " + note.Country);
			if (note.BirthdayDate != null)
			{
				Console.WriteLine("Дата рождения: " + ((DateTime)note.BirthdayDate).ToString("dd.MM.yyyy"));
			}
			else
				Console.WriteLine("Дата рождения: " + note.BirthdayDate);
			Console.WriteLine("Организация: " + note.Organization);
			Console.WriteLine("Должность: " + note.Position);
			Console.WriteLine("Другие заметки: " + note.Other);
		}

		static void ShowAllNotes(List<Note> notes)
		{
			if (notes.Count == 0)
				Console.WriteLine("Записная книжка пуста.");
			else
				foreach (Note note in notes)
				{
					Console.WriteLine();
					Console.WriteLine("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
					Console.WriteLine();
					Console.WriteLine("Идентификатор: " + note.Id);
					Console.WriteLine("Фамилия: " + note.Surname);
					Console.WriteLine("Имя: " + note.Name);
					Console.WriteLine("Номер телефона: " + note.Telephone);
				}
		}
	}
}

